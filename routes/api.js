var express = require('express');
var router = express.Router();
var pg = require('pg');
var cadenaDeConexion = process.env.DATABASE_URL || 'postgres://postgres:123@localhost:5432/db_hearhereApi';
//var oauth=require('../private/middleware');





 router.get('/saludar', function(req, res,next) {
  res.send("SALUDANDO DESDE API");
});

/* GET 
	/api/perfilApi
 */

 router.get('/perfilApi', function(req, res,next) {
 
  var resultado=[];
  
  pg.connect(cadenaDeConexion,function(err,db,done){
	var query=db.query("select * from perfilApi");
	query.on('row',function(row){
		resultado.push(row);	
	});
	query.on('end',function(){
		db.end();
		console.log(res.json(resultado));
		return res.json(resultado);
	});
	if(err){
		console.log(err);		
	}
  });
});


router.post('/perfilApi', function(req, res) {
  var resultado=[];
  var data={usuarioApi: req.body.usuarioApi, pass:req.body.pass, nombre:req.body.nombre, foto:req.body.foto,  ubicacion:req.body.ubicacion};
  
  console.log("USUARIO: "+data.usuarioApi);
  pg.connect(cadenaDeConexion,function(err,db,done){
	db.query('update perfilApi set usuarioApi=$0, pass=$1, nombre=$3, foto=$4,ubicacion=$5 where perfilApi.usuarioApi=$6',[data.usuarioApi,data.pass,data.nombre,data.foto,data.ubicacion,data.usuarioApi]);
	var query=db.query("select * from perfilApi");
	query.on('row',function(row){
		resultado.push(row);		
	});
	query.on('end',function(){
		db.end();
		console.log(res.json(resultado));
		return res.json(resultado);
	});
	if(err){
		console.log(err);		
	}
  });
});
router.delete('/usuario',function(req,res){
	var resultado=[];
	var data={idUsuario:req.body.idUsuario,nombre: req.body.nombre,nick:req.body.nick,contrasena:req.body.contrasena,correo:req.body.correo,telefono:req.body.telefono};
    pg.connect(cadenaDeConexion,function(err,db,done){
	db.query('update usuario set nombre=$1,nick=$2,contrasena=$3,correo=$4,telefono=$5 where usuario.idUsuario=$6',[data.nombre,data.nick,data.contrasena,data.correo,data.telefono,data.idUsuario]);
	var query=db.query("select * from usuario");
	query.on('row',function(row){
		resultado.push(row);		
	});
	query.on('end',function(){
		db.end();
		console.log(res.json(resultado));
		return res.json(resultado);
	});
	if(err){
		console.log(err);		
	}
  });
});

router.put('/usuario',function(req,res){
	var resultado=[];
	var data={idUsuario:req.body.idUsuario};
    pg.connect(cadenaDeConexion,function(err,db,done){
	db.query('delete from usuario where usuario.idUsuario=$1',[data.idUsuario]);
	var query=db.query("select * from usuario");
	query.on('row',function(row){
		resultado.push(row);		
	});
	query.on('end',function(){
		db.end();
		console.log(res.json(resultado));
		return res.json(resultado);
	});
	if(err){
		console.log(err);		
	}
  });
});


module.exports = router;
